package websocket.websocket;

import java.io.IOException;
import java.net.URI;
import java.util.Scanner;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.websocket.ContainerProvider;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

import org.glassfish.tyrus.client.ClientManager;

public class WSClient {

    final static CountDownLatch messageLatch = new CountDownLatch(1);

    public static void main(String[] args) throws Exception {
        ClientManager client = ClientManager.createClient();
        String message;
        String uri = "ws://localhost:8025/ws/test";
        // connect to server
        Scanner scanner = new Scanner(System.in);
        System.out.println("test Msg Client");
        String user = scanner.nextLine();
        Session session = client.connectToServer(WSClientEndpoint.class, new URI(uri));
        System.out.println("Name is " + user);

        // repeatedly read a message and send it to the server (until quit)
        do {
            message = scanner.nextLine(); /// Read File and send to server end point.
            session.getBasicRemote().sendText(message);
            
        } while (!message.equalsIgnoreCase("quit"));
    }
    
    public static void main1(String[] args) {
        try {
            WebSocketContainer container = ContainerProvider.getWebSocketContainer();
            String uri = "ws://localhost:8025/ws/test";
            System.out.println("Connecting to " + uri);
            container.connectToServer(WSClientEndpoint.class, URI.create(uri));
            messageLatch.await(100, TimeUnit.SECONDS);
        } catch (IOException ex) {
            Logger.getLogger(WSClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex){
        	Logger.getLogger(WSClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}