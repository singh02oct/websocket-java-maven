package websocket.websocket;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.websocket.EncodeException;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint(value = "/mydata")
public class WSServerEndPoint {
	// This object will add the users or client in set and we can use it in future to check how manny clients are connected with it
	// TODO : Need to check how session works... 
    static Set<Session> peers = Collections.synchronizedSet(new HashSet<Session>());

    // Method calls when any connection open.
    @OnOpen
    public void onOpen(Session session) {
        System.out.println("Connection Open, Session ID - " + session.getId());
        peers.add(session);
    }
 // Method calls when message recive from client.
    @OnMessage
    public void onMessage(String message, Session session) throws IOException, EncodeException {
        //broadcast the message
    	System.out.println("On Recive Message, Session ID :- " + session.getId());
    	System.out.println("On Recive Message, Message :- " + message);
    	// Send message to all the connected clients.
    	session.getBasicRemote().sendObject("My Msg from Server data get it");
    	for (Session peer : peers) {
            if (!session.getId().equals(peer.getId())) {
                peer.getBasicRemote().sendObject("Msg from Server 1111");
            }
        }
    }

    // Method calls when any connection cloe.
    @OnClose
    public void onClose(Session session) throws IOException, EncodeException {
        System.out.println(" Connection Closed, Session ID :- " + session.getId());
        peers.remove(session);
        for (Session peer : peers) {
            peer.getBasicRemote().sendObject("Session closed, Brodcast to all clients. "); // Testing
        }
    }

}