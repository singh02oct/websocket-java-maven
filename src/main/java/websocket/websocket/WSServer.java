package websocket.websocket;

import java.util.Scanner;

import javax.websocket.DeploymentException;

import org.glassfish.tyrus.server.Server;

/**
 *
 */
public class WSServer {
	public static void main(String[] args) {
		System.out.println("Testing");

		Server server = new Server("localhost", 8025, "/ws", WSServerEndPoint.class);
		try {
			server.start();
			System.out.println("Press any key to stop the server..");
			new Scanner(System.in).nextLine();
		} catch (DeploymentException e) {
			throw new RuntimeException(e);
		} finally {
			server.stop();
		}
	}
}
